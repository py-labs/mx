class RoomsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  # GET /rooms
  # GET /rooms.json
  def index
    @rooms = Room.all
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
  end

  # GET /rooms/new
  def new
    @room = Room.new
  end

  # GET /rooms/1/edit
  def edit
  end

  # POST /rooms
  # POST /rooms.json
  def create
    @room = Room.new(room_params)

    respond_to do |format|
      if @room.save
        format.html { redirect_to @room, notice: 'Room was successfully created.' }
        format.json { render action: 'show', status: :created, location: @room }
      else
        format.html { render action: 'new' }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  def add
    key  = ("a".."z").to_a.shuffle.join[1..8]
    room = Room.new(:key => key)
    film = Film.find(params[:id])
    room.user = current_user
    film.room = room
    film.save
    
    #
    render :json => film.room
  end

  def join
    room = Room.find_one(:key => params[:key])
    @film = room.film
    room.users << current_user
    room.save
  end

  def admin_event
    room = Room.find_one(:key => params[:key])
    chat = Chat.last
    render :json => [room.users, room.user, chat]
  end

  def user_event
    chat = Chat.last
    room = Room.find_one(:key => params[:key])
    render :json => [room.user, room.users, chat]
  end

  def send_message
    chat = Chat.new(params)
    chat.save
    render :json => chat
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    respond_to do |format|
      if @room.update(room_params)
        format.html { redirect_to @room, notice: 'Room was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    @room.destroy
    respond_to do |format|
      format.html { redirect_to rooms_url }
      format.json { head :no_content }
    end
  end

  private
    def message(type, message)

    end
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:key, :admin)
    end
end
