json.array!(@categories) do |category|
  json.extract! category, :name, :about
  json.url category_url(category, format: :json)
end
