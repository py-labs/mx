json.array!(@films) do |film|
  json.extract! film, :name, :file
  json.url film_url(film, format: :json)
end
