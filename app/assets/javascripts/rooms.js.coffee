# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@myPlayer = window.myPlayer ? {}

add_message = (data) -> 
  if data['id'] != Cookies 'id' 
    Cookies.set 'last_message', data['id']
    $("#online_chat").append(data['message'])

command = (data) ->
  command  = data[1..]
  switch command
    when "play"
      myPlayer.play()
    when "pause"
      myPlayer.pause()
    when "paused"
      myPlayer.paused()

$(document).ready ->
  window.myPlayer = videojs("room_video")
  $('#create_room').click (event) ->
    event.preventDefault()

    console.log this.hash[1..24]
    $.ajax
      type: "POST"
      url: '/room/add'     
      data:
        id: this.hash[1..24]
      beforeSend: (data) ->
        console.log "beforeSend"
      success: (data) ->
        console.log "success"
        $('#room_link input').attr('value', "http://#{location.hostname}/room/join/#{data['key']}")
        Cookies.set('key', data['key'])

        $('#create_room').attr('disabled', 'true')

        setInterval (->
          $.ajax
            type: "get"
            url: "/room/admin_event/#{Cookies('key')}"
            success: (data) ->
              if Cookies('last_message') != data[2]['id']              
                Cookies.set 'last_message', data[2]['id']
                if data[2]['message'][0] == '/'
                  command(data[2]['message'])
                else
                  $("#online_chat").append("<p><b> ></b> #{data[2]['message']}</p>")
                  $("form#text-form input").val('')

              count_user = data.length
              if count_user != 0
                html = ""
                html += [
                  "<li class='active'>",
                    "<a href='#'>",
                      "#{data[1]['email']}",
                    "</a>",
                  "</li>"].join("")
                $.each data[0], (id, session) ->
                  html += [
                    "<li id='#{session['id']}'>",
                      "<a href='#'>",
                        session['email'],
                      "</a>",
                    "</li>"].join("")
                $("#online_users").html(html)
                $("#start_view").removeAttr('disabled')
                $("form#text-form button").prop("disabled", false)
                $("form#text-form input").prop("disabled", false)
        ), 1000

  $('#connect_room').click (event) ->
    event.preventDefault()
    key = this.baseURI.split('/')[5]
    Cookies.set('key', key)
    setInterval (->
      $.ajax
        type: "get"
        url: "/room/user_event/#{Cookies('key')}"
        success: (data) ->
          if Cookies('last_message') != data[2]['id']              
            Cookies.set 'last_message', data[2]['id']
            if data[2]['message'][0] == '/'
              command(data[2]['message'], )
            else
              $("#online_chat").append("<p><b> ></b> #{data[2]['message']}</p>")
              $("form#text-form input").val('')

          count_user = data.length
          if count_user != 0
            html = ""
            html += [
              "<li class='active' >",
                "<a href='#' >",
                  "#{data[0]['email']}",
                "</a>",
              "</li>"].join("")
            $.each data[1], (id, session) ->
              html += [
                "<li id='#{session['id']}'>",
                  "<a href='#'>",
                    session['email'],
                  "</a>",
                "</li>"].join("")
            $("#online_users").html(html)
            $("#start_view").attr('disabled', 'false')
            $("form#text-form button").prop("disabled", false)
            $("form#text-form input").prop("disabled", false)
    ), 1000
   
  $("form#text-form").submit((e) ->
    form = $(e.target)
    e.preventDefault()
    #@repair!
    $.ajax
      type: "POST"
      url: "/room/send_message"    
      data: form.serialize()
      success: (data) ->
    )

  $("#start_view").click (e) ->
    $.ajax
      type: "POST"
      url: "/room/send_message"    
      data: 
        message: "/play"
      success: (data) ->

  $("#stop_view").click (e) ->
    $.ajax
      type: "POST"
      url: "/room/send_message"    
      data: 
        message: "/play"
      success: (data) ->

  





