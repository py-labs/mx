class Film
  include MongoMapper::Document

  key :name, String
  key :file, String

  one :room

end
