class Category
  include MongoMapper::Document

  key :name, String
  key :about, String

end
