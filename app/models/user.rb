class User
  include MongoMapper::Document
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  key :email,              String
  key :encrypted_password, String
  key :current_sign_in_ip, String
  key :last_sign_in_ip,    String
  key :sign_in_count,      Integer
  key :current_sign_in_at, Time
  key :last_sign_in_at,    Time
  key :remember_created_at,Time

  attr_accessible :email, :password, :password_confirmation, :encrypted_password, :last_sign_in_at, :current_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :sign_in_count, :remember_created_at

  many :rooms

end
