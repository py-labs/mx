class Room
  include MongoMapper::Document

  key :key,       String
  key :users_ids, Array

  belongs_to :film
  many :users, :in => :users_ids
  belongs_to :user
  

end
