class Chat
  include MongoMapper::Document

  key :message, String
  timestamps!

end
